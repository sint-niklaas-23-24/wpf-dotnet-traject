﻿using System.Text.RegularExpressions;
using System.Windows;

namespace WPF___DotNet_Traject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Click_BtnOpslaan(object sender, RoutedEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtAchternaam.Text) || String.IsNullOrEmpty(txtVoornaam.Text))
                {
                    MessageBox.Show("U moet alle textboxen invulen", "Foutmeldin", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    if ((Regex.Match(txtVoornaam.Text, "^[A-Z][a-zA-Z]*$").Success) && (Regex.Match(txtAchternaam.Text, "^[A-Z][a-zA-Z]*$").Success))
                    {
                        MessageBox.Show("Welkom " + txtVoornaam.Text + " " + txtAchternaam.Text + "!", ".Net traject", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("De namen mogen niet cijfers zijn!", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);

                    }

                }
                txtAchternaam.Text = txtVoornaam.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
    }
}